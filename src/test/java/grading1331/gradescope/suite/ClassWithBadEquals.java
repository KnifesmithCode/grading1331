package grading1331.gradescope.suite;

/**
 * Testing Class for Testing Grading Utils.
 * @author Andrew Chafos
 * @version 1.0
 */
public class ClassWithBadEquals {

    /**
     * Overloaded version of the equals method for Checkstyle to complain about.
     * @param otherMe some other ClassWithBadEquals method
     * @return true if otherMe is null, false otherwise
     */
    public boolean equals(ClassWithBadEquals otherMe) {
        return null == otherMe;
    }

}

class AnotherClassWithBadEquals {
    @Override
    public boolean equals(Object other) {
        return false;
    }
}
