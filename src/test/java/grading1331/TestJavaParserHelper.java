package grading1331;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;
import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.List;

public class TestJavaParserHelper {

    @Test
    public void testGetAnnotations() throws FileNotFoundException{
        CompilationUnit exampleClass = JavaParser.parse(new File("./src/test/java/testutil/ExampleArbitraryClass.java"));
        Map<MethodDeclaration, List<AnnotationExpr>> annotations = JavaParserHelper.walkMethodAnnotations(exampleClass);
        assertTrue(annotations.size() == 2);
        for (MethodDeclaration method : annotations.keySet()) {
            // equals has 2 annotations and the other one has 1 annotation
            if (method.getName().asString().equals("equals")) {
                assertTrue(annotations.get(method).size() == 2, "Equals has wrong number of annotations");
            } else if (method.getName().asString().equals("timesByRandom")) {
                assertTrue(annotations.get(method).size() == 1, "timesByRandom has wrong number of annotations");
            } else {
                fail("Method shouldn't have annotation: " + method);
            }
        }
    }

    @Test
    public void testAssertAnnotations() throws FileNotFoundException{
        CompilationUnit exampleClass = JavaParser.parse(new File("./src/test/java/testutil/ExampleArbitraryClass.java"));
        JavaParserHelper.assertAnnotation(exampleClass, "Override", "equals", "Object");
    }

    @Test
    public void testAssertAnnotationsNoMethod() throws FileNotFoundException{
        CompilationUnit exampleClass = JavaParser.parse(new File("./src/test/java/testutil/ExampleArbitraryClass.java"));
        assertThrows(AssertionError.class,
            () -> JavaParserHelper.assertAnnotation(exampleClass, "Override", "equalssssssssssssss")
        );
    }

    @Test
    public void testAssertAnnotationsMissingAnnotation() throws FileNotFoundException{
        CompilationUnit exampleClass = JavaParser.parse(new File("./src/test/java/testutil/ExampleArbitraryClass.java"));
        assertThrows(AssertionError.class,
            () -> JavaParserHelper.assertAnnotation(exampleClass, "Fake", "equals", "Object")
        );
    }

}
