package grading1331.builder;

import grading1331.gradescope.TestResult;
import grading1331.gradescope.Visibility;

/**
 * A wrapper for a Runnable object which contains some test to be run by {@link grading1331.gradescope.GradedTestRunner}
 * as well as a wrapper TestResult object used to create the JSON output with the results of said test
 * @author Andrew Chafos
 */
public class RunnableTest {
    private final TestResult testWrapper;
    private final Runnable test;

    /**
     * Creates a RunnableTest object with the supplied Runnable and a TestResult created from the other supplied
     * parameters
     * @param name the name to be supplied to the TestResult
     * @param number the number to be supplied to the TestResult
     * @param maxScore the maxScore to be supplied to the TestResult
     * @param visibility the visibility to be supplied to the TestResult
     * @param test the Runnable object to be accordingly assigned to its corresponding instance field
     */
    public RunnableTest(String name, String number, double maxScore, Visibility visibility, Runnable test) {
        this.testWrapper = new TestResult(name, number, maxScore, visibility);
        this.test = test;
    }

    /**
     * Returns the Runnable object wrapped by this class
     * @return the Runnable object as described above
     */
    public Runnable getRunnable() {
        return test;
    }

    /**
     * Returns the TestResult object wrapped by this class
     * @return the TestResult object as described above
     */
    public TestResult getTestWrapper() {
        return testWrapper;
    }
}
