# grading1331 library

This is the CS 1331 grading utilities library.

This library consists of many source files that will be periodically built into a JAR file,
which contains not only useful utilities related to Java Reflection and obtaining properties
of Java structures, but also a miniature framework used to run a series of methods with
a @GradedTest annotation as though it was a JUnit suite and then convert the results into
a JSON format accepted by Gradescope and consistent with the results of the tests.

## This project uses the Gradle build tool.

In order to build this project, you must first [install gradle](https://gradle.org/install/).

To see if you installed Gradle correctly, you can run the following command:
```bash
gradle -v
```

Next, you can see if the current version of the project builds and passes all unit tests by running:
```bash
gradle build
```

Finally, this command will output a JAR-file of all source files into the root directory:
```bash
gradle jar
```

## Javadocs

You can use the following command to generate JavaDocs for this project:
```bash
gradle javadoc
```

## To do
There are still many things which we could stand to add to this library. Some of these have
been created as Issues in this GitLab repository, but some others are listed here for
historical documentation purposes.  These additions include the following:
- Asserts and tests for arbitrary collections
  - for example, to test size on some random collection just need to know what
  method to invoke, and how to add/remove if the student add/removes don't work.
  e.g:
  ```java
  <T> void testSize(IntSupplier sizeMethod, Consumer<T> studentAdd, Supplier<T> studentRemove, Consumer<T> manualAdd, Supplier<T> manualRemove, Object target)
  ```
  - It is theoretically possible to construct these to work on any arbitrary
  collections, but at a minimum we should be able to make this work for
  array-backed collections provided the size and backing array field names are
  known
- Polymorphism and inheritance assertions in Miscellaneous
- Tests related to Comparable and other standard interfaces in Miscellaneous
  - Given some dummy instances and a correct Comparator, we can fully test
  Comparable as well
- Generics type testing with `javap`

In addition, having examples for the following:
- Testing if a method is called/overriden by extending the student class
or impersonating the class they are calling and checking if it was called
- Testing if they constructor chain/delegate correctly using a modified version
of a provided superclass on gradescope and parsing the stack by sampling the
stack using Thread/an Exception